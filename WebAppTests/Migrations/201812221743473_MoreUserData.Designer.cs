// <auto-generated />
namespace WebAppTests.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class MoreUserData : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(MoreUserData));
        
        string IMigrationMetadata.Id
        {
            get { return "201812221743473_MoreUserData"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
