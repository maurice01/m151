namespace WebAppTests.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoreUserData : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "vorname", c => c.String());
            AddColumn("dbo.AspNetUsers", "nachname", c => c.String());
            AddColumn("dbo.AspNetUsers", "adresse", c => c.String());
            AddColumn("dbo.AspNetUsers", "plz", c => c.String());
            AddColumn("dbo.AspNetUsers", "ort", c => c.String());
            AddColumn("dbo.AspNetUsers", "telefon", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "telefon");
            DropColumn("dbo.AspNetUsers", "ort");
            DropColumn("dbo.AspNetUsers", "plz");
            DropColumn("dbo.AspNetUsers", "adresse");
            DropColumn("dbo.AspNetUsers", "nachname");
            DropColumn("dbo.AspNetUsers", "vorname");
        }
    }
}
