﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppTests
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string userName = Context.User.Identity.GetUserName();
            if(userName.Count() == 0)
            {
                Response.Redirect("Account/Login");
            }
            else
            {
                Response.Redirect("Account/Events");
            }
        }
    }
}