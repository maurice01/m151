//------------------------------------------------------------------------------
// <auto-generated>
//     Der Code wurde von einer Vorlage generiert.
//
//     Manuelle Änderungen an dieser Datei führen möglicherweise zu unerwartetem Verhalten der Anwendung.
//     Manuelle Änderungen an dieser Datei werden überschrieben, wenn der Code neu generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebAppTests.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Event
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Event()
        {
            this.Fahrzeug = new HashSet<Fahrzeug>();
            this.Mitfahrgelegenheit = new HashSet<Mitfahrgelegenheit>();
        }
    
        public int Id { get; set; }
        public string Bezeichnung { get; set; }
        public System.DateTime Datum { get; set; }
        public string Uhrzeit { get; set; }
        public string Ort { get; set; }
        public int FK_Verein { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Fahrzeug> Fahrzeug { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Mitfahrgelegenheit> Mitfahrgelegenheit { get; set; }
        public virtual Verein Verein { get; set; }
    }
}
