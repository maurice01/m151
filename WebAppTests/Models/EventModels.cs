﻿using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using WebAppTests.Models;

//namespace WebAppTests.Models
//{
//    // Sie können Benutzerdaten für den Benutzer durch Hinzufügen weiterer Eigenschaften zur User-Klasse hinzufügen. Weitere Informationen finden Sie unter "http://go.microsoft.com/fwlink/?LinkID=317594".
//    public class Event
//    {
//        public string vorname { get; set; }
//        public string nachname { get; set; }
//        public string adresse { get; set; }
//        public string plz { get; set; }
//        public string ort { get; set; }
//        public string telefon { get; set; }

//    }

//    public class EventDbContext : DbContext
//    {
//        public EventDbContext()
//            : base("DefaultConnection")
//        {
//        }

//        public static EventDbContext Create()
//        {
//            return new EventDbContext();
//        }
//    }
//}

//#region Hilfsprogramme
//namespace WebAppTests
//{
//    public static class IdentityHelper
//    {
//        // Wird für XSRF beim Verknüpfen externer Anmeldungen verwendet.
//        public const string XsrfKey = "XsrfId";

//        public const string ProviderNameKey = "providerName";
//        public static string GetProviderNameFromRequest(HttpRequest request)
//        {
//            return request.QueryString[ProviderNameKey];
//        }

//        public const string CodeKey = "code";
//        public static string GetCodeFromRequest(HttpRequest request)
//        {
//            return request.QueryString[CodeKey];
//        }

//        public const string UserIdKey = "userId";
//        public static string GetUserIdFromRequest(HttpRequest request)
//        {
//            return HttpUtility.UrlDecode(request.QueryString[UserIdKey]);
//        }

//        public static string GetResetPasswordRedirectUrl(string code, HttpRequest request)
//        {
//            var absoluteUri = "/Account/ResetPassword?" + CodeKey + "=" + HttpUtility.UrlEncode(code);
//            return new Uri(request.Url, absoluteUri).AbsoluteUri.ToString();
//        }

//        public static string GetUserConfirmationRedirectUrl(string code, string userId, HttpRequest request)
//        {
//            var absoluteUri = "/Account/Confirm?" + CodeKey + "=" + HttpUtility.UrlEncode(code) + "&" + UserIdKey + "=" + HttpUtility.UrlEncode(userId);
//            return new Uri(request.Url, absoluteUri).AbsoluteUri.ToString();
//        }

//        private static bool IsLocalUrl(string url)
//        {
//            return !string.IsNullOrEmpty(url) && ((url[0] == '/' && (url.Length == 1 || (url[1] != '/' && url[1] != '\\'))) || (url.Length > 1 && url[0] == '~' && url[1] == '/'));
//        }

//        public static void RedirectToReturnUrl(string returnUrl, HttpResponse response)
//        {
//            if (!String.IsNullOrEmpty(returnUrl) && IsLocalUrl(returnUrl))
//            {
//                response.Redirect(returnUrl);
//            }
//            else
//            {
//                response.Redirect("~/");
//            }
//        }
//    }
//}
//#endregion
