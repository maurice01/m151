//------------------------------------------------------------------------------
// <auto-generated>
//     Der Code wurde von einer Vorlage generiert.
//
//     Manuelle Änderungen an dieser Datei führen möglicherweise zu unerwartetem Verhalten der Anwendung.
//     Manuelle Änderungen an dieser Datei werden überschrieben, wenn der Code neu generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebAppTests.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Vereinsmitgliedschaft
    {
        public int Id { get; set; }
        public int FK_Verein { get; set; }
        public int FK_Vereinsmitglied { get; set; }
    
        public virtual Verein Verein { get; set; }
        public virtual Vereinsmitglied Vereinsmitglied { get; set; }
    }
}
