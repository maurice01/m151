﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace WebAppTests.Account
{
    public partial class FahrzeugHinzufuegen : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["PreviousPageURL"] = Request.UrlReferrer.ToString();
            }
        }

        protected void Speichern_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["EventId"]))
            {
                string eventId = Request.QueryString["EventId"];
                string userId = Context.User.Identity.GetUserId();
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var userX = manager.FindById(userId);
                var realUserId = userX.Id;

                XmlDocument doc = new XmlDocument();
                doc.Load(Server.MapPath("~/App_Data/Fahrzeuge.xml"));
                XmlElement row = doc.CreateElement("row");
                XmlElement id = doc.CreateElement("Id");
                id.InnerText = "19";
                XmlElement user = doc.CreateElement("UserId");
                user.InnerText = realUserId;
                XmlElement sitzplaetzeEl = doc.CreateElement("Plaetze");
                sitzplaetzeEl.InnerText = Sitzplaetze.Text;
                XmlElement ortEl = doc.CreateElement("Ort");
                ortEl.InnerText = Ort.Text;
                XmlElement adresseEl = doc.CreateElement("Adresse");
                adresseEl.InnerText = Adresse.Text;
                XmlElement datumEl = doc.CreateElement("Datum");
                datumEl.InnerText = Datum.Text;
                XmlElement uhrzeitEl = doc.CreateElement("Uhrzeit");
                uhrzeitEl.InnerText = Uhrzeit.Text;
                XmlElement eventIdEl = doc.CreateElement("EventId");
                eventIdEl.InnerText = eventId;
                row.AppendChild(id);
                row.AppendChild(user);
                row.AppendChild(sitzplaetzeEl);
                row.AppendChild(ortEl);
                row.AppendChild(adresseEl);
                row.AppendChild(datumEl);
                row.AppendChild(uhrzeitEl);
                row.AppendChild(eventIdEl);

                doc.DocumentElement.AppendChild(row);
                doc.Save(Server.MapPath("~/App_Data/Fahrzeuge.xml"));
                Response.Redirect(ViewState["PreviousPageURL"].ToString());
            }
            else
                Response.Redirect("/");
        }
        protected void Abbrechen_Click(object sender, EventArgs e)
        {
            Response.Redirect(ViewState["PreviousPageURL"].ToString());
        }

    }
}