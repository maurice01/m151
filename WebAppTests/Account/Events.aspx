﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Events.aspx.cs" Inherits="WebAppTests.Account.Events" EnableEventValidation = "false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Events</h2>
    <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
        <p class="text-danger">
            <asp:Literal runat="server" ID="FailureText" />
        </p>
    </asp:PlaceHolder>
    <asp:GridView ID="EventGV" runat="server" OnSelectedIndexChanged="EventGV_SelectedIndexChanged" OnRowDataBound="EventGV_RowDataBound" OnPreRender="EventGV_PreRender"></asp:GridView>  

</asp:Content>
