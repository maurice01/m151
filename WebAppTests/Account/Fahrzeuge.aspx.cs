﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using WebAppTests.Models;

namespace WebAppTests.Account
{
    public partial class Fahrzeuge : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["EventId"]))
                {
                    string eventId = Request.QueryString["EventId"];
                    BindDataToGridView(eventId);
                    selectUsedCar();
                }
            }
        }

        protected void BindDataToGridView(string EventId)
        {
            using (var db = new MitfahrgelegenheitEntities())
            {
                var intEventId = int.Parse(EventId);
                var query = from f in db.Fahrzeug where f.Id == intEventId select f;
                var data = query.ToList();
                if (data.Count() > 0)
                {
                    FahrzeugGV.DataSource = data;
                    FahrzeugGV.DataBind();
                }
                else
                {
                    // Keine Fahrzeuge in diesem Event!
                }
            }
        }

        protected void FahrzeugGV_RowDataBound(object sender, GridViewRowEventArgs e)
        {}

        protected void SelectCheckBox_OnCheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = sender as CheckBox;

            if (chk.Checked)
            {
                foreach(GridViewRow gvr in FahrzeugGV.Rows)
                {
                    CheckBox checkBox = ((CheckBox)gvr.FindControl("SelectCheckBox"));
                    if (checkBox == chk)
                        continue;
                    bool isChecked = checkBox.Checked;
                    if (isChecked)
                    {
                        chk.Checked = false;
                        break;
                    }
                }
                if (chk.Checked)
                {
                    if (IsUserInACar().Equals(string.Empty))
                        buttonJoin.Visible = true;
                    else
                        buttonJoin.Visible = false;
                }
            }
            else
            {
                buttonJoin.Visible = false;
            }

        }

        protected void FahrzeugHinzufuegen_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["EventId"]))
            {
                string eventId = Request.QueryString["EventId"];
                Response.Redirect("/Account/FahrzeugHinzufuegen.aspx?EventId=" + eventId);
            }
        }

        protected void FahrzeugBeitreten_Click(object sender, EventArgs e)
        {
            string FahrzeugId = String.Empty;
            foreach (GridViewRow gvr in FahrzeugGV.Rows)
            {
                CheckBox checkBox = ((CheckBox)gvr.FindControl("SelectCheckBox"));
                bool isChecked = checkBox.Checked;
                if (isChecked)
                {
                    FahrzeugId = gvr.Cells[1].Text;
                }
            }

            string userId = Context.User.Identity.GetUserId();
            XmlDocument doc = new XmlDocument();
            doc.Load(Server.MapPath("~/App_Data/Fahrzeug_User.xml"));
            XmlElement rowEl = doc.CreateElement("row");
            XmlElement userIdEl = doc.CreateElement("userId");
            userIdEl.InnerText = userId;
            XmlElement fahrzeugIdEl = doc.CreateElement("FahrzeugId");
            fahrzeugIdEl.InnerText = FahrzeugId;
            rowEl.AppendChild(userIdEl);
            rowEl.AppendChild(fahrzeugIdEl);
            doc.DocumentElement.AppendChild(rowEl);
            doc.Save(Server.MapPath("~/App_Data/Fahrzeug_User.xml"));
            selectUsedCar();
        }

        protected string IsUserInACar()
        {
            string userId = Context.User.Identity.GetUserId();
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = manager.FindById(userId);
            

            using (var db = new MitfahrgelegenheitEntities())
            {
                var userEmail = user.Email;
                var searchedUser = (from u in db.Vereinsmitglied where u.Email == userEmail select u).First();
                var eventId = int.Parse(Request.QueryString["EventId"]);
                var car = from mfg in db.Mitfahrgelegenheit where mfg.FK_Vereinsmitglied == searchedUser.Id && mfg.FK_Event == eventId select mfg;
            };

            XmlTextReader fahrzeugReader = new XmlTextReader(Server.MapPath("~/App_Data/Fahrzeuge.xml"));
            DataSet fahrzeugData = new DataSet();
            fahrzeugData.ReadXml(fahrzeugReader);
            fahrzeugReader.Close();
            if(fahrzeugData.Tables.Count != 0)
            {
                foreach(DataRow fahrzeugRow in fahrzeugData.Tables[0].Rows)
                {
                    if(fahrzeugRow[1].ToString() == userId)
                    {
                        XmlTextReader fahrzeug_user_reader = new XmlTextReader(Server.MapPath("~/App_Data/Fahrzeug_User.xml"));
                        DataSet fahrzeug_user_data = new DataSet();
                        fahrzeug_user_data.ReadXml(fahrzeug_user_reader);
                        fahrzeug_user_reader.Close();
                        if (fahrzeug_user_data.Tables.Count != 0)
                        {
                            DataTable fahrzeug_user_tabelle = fahrzeug_user_data.Tables[0];
                            foreach (DataRow row in fahrzeug_user_tabelle.Rows)
                            {
                                if(fahrzeugRow[0].ToString() == row[1].ToString())
                                { // Fahrzeug und User stimmen überein -> er ist schon beigetreten
                                    return fahrzeugRow[0].ToString();
                                }
                            }
                        }
                    }
                }
            }
            return string.Empty;
        }

        protected void selectUsedCar()
        {
            if(IsUserInACar() != string.Empty)
            {
                string fahrzeugId = IsUserInACar();
                foreach (GridViewRow row in FahrzeugGV.Rows)
                {
                    if(row.Cells[1].Text.Equals(fahrzeugId))
                    {
                        row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                        row.ToolTip = "Sie sind diesem Fahrzeug bereits beigetreten.";
                        buttonJoin.Visible = false;
                        break;
                    }
                    else
                    {
                        row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                        row.ToolTip = string.Empty;
                    }
                }
            }
        }
    }
}