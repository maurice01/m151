﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using WebAppTests.Models;

namespace WebAppTests.Account
{
    public partial class Events : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDataToGridView();
            }
            else
            {
                
            }
        }

        protected void BindDataToGridView()
        {
            using(var db = new MitfahrgelegenheitEntities())
            {
                var query = from e in db.Event select e;
                var data = query.ToList();
                if(data.Count() > 0)
                {
                    EventGV.DataSource = data;
                    EventGV.DataBind();
                }
            }


            //XmlTextReader xmlReader = new XmlTextReader(Server.MapPath("~/App_Data/Events.xml"));
            //DataSet data = new DataSet();
            //data.ReadXml(xmlReader);
            //xmlReader.Close();

            //if(data.Tables.Count != 0)
            //{
            //    EventGV.DataSource = data;
            //    EventGV.DataBind();
            //}
            //else
            //{
            //    //keine Daten!!!
            //}
        }

        protected void EventGV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(EventGV, "Select$" + e.Row.RowIndex);
                e.Row.Attributes.Add("onmouseover", "this.style.cursor='pointer'");
            }
        }

        protected void EventGV_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in EventGV.Rows)
            {
                if (row.RowIndex == EventGV.SelectedIndex)
                {
                    TableCell cell = row.Cells[0];
                    string eventId = cell.Text;

                    Response.Redirect("/Account/Fahrzeuge.aspx?EventId=" + eventId);
                    break;
                }
            }
        }

        protected void EventGV_PreRender(object sender, EventArgs e)
        {
            //if (EventGV.Columns.Count > 0)
            //    EventGV.Columns[0].Visible = false;
            //else
            //{
            //    EventGV.HeaderRow.Cells[0].Visible = false;
            //    foreach (GridViewRow gvr in EventGV.Rows)
            //    {
            //        gvr.Cells[1].Visible = false;
            //    }
            //}
        }

    }
}