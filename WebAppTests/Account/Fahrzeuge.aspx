﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Fahrzeuge.aspx.cs" Inherits="WebAppTests.Account.Fahrzeuge" EnableEventValidation = "false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <hr />
    <asp:Button ID="buttonAdd" runat="server" OnClick="FahrzeugHinzufuegen_Click" Text="Fahrzeug hinzufügen" CssClass="btn btn-default" />
    <asp:Button ID="buttonJoin" runat="server" OnClick="FahrzeugBeitreten_Click" Text="Fahrzeug beitreten" CssClass="btn btn-default" Visible="false" />
    <hr />
    <h2>Fahrzeuge</h2>
    <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
        <p class="text-danger">
            <asp:Literal runat="server" ID="FailureText" />
        </p>
    </asp:PlaceHolder>
    <asp:GridView ID="FahrzeugGV" runat="server" OnRowDataBound="FahrzeugGV_RowDataBound">
        <Columns>
            <asp:TemplateField>
            <ItemTemplate>
               <asp:CheckBox ID="SelectCheckBox" runat="server" AutoPostBack="true" OnCheckedChanged="SelectCheckBox_OnCheckedChanged"/>
            </ItemTemplate>
        </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
