﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FahrzeugHinzufuegen.aspx.cs" Inherits="WebAppTests.Account.FahrzeugHinzufuegen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="form-horizontal">
        <h4>Neues Konto erstellen</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Sitzplaetze" CssClass="col-md-2 control-label">Sitzplätze</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Sitzplaetze" CssClass="form-control" TextMode="Number" min="1" max="16" step="1" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Sitzplaetze"
                    CssClass="text-danger" ErrorMessage="Das Sitzplätze-Feld ist erforderlich." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Datum" CssClass="col-md-2 control-label">Datum</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Datum" CssClass="form-control" TextMode="Date" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Datum"
                    CssClass="text-danger" ErrorMessage="Das Datum-Feld ist erforderlich." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Uhrzeit" CssClass="col-md-2 control-label">Uhrzeit</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Uhrzeit" CssClass="form-control" TextMode="SingleLine" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Uhrzeit"
                    CssClass="text-danger" ErrorMessage="Das Uhrzeit-Feld ist erforderlich." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Adresse" CssClass="col-md-2 control-label">Adresse</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Adresse" CssClass="form-control" TextMode="SingleLine" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Adresse"
                    CssClass="text-danger" ErrorMessage="Das Adresse-Feld ist erforderlich." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Ort" CssClass="col-md-2 control-label">Ort</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Ort" CssClass="form-control" TextMode="Phone" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Ort"
                    CssClass="text-danger" ErrorMessage="Das Ort-Feld ist erforderlich." />
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="Speichern_Click" Text="Speichern" CssClass="btn btn-default" />
                <asp:Button runat="server" OnClick="Abbrechen_Click" Text="Abbrechen" CssClass="btn btn-default" CausesValidation="false" />
            </div>
        </div>
    </div>

</asp:Content>
